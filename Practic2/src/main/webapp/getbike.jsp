<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Выбор Велосипеда</title>
<style>
    body { font-family: Arial, sans-serif; line-height: 1.6; }
    .container { width: 80%; margin: auto; overflow: hidden; }
    header { background: #333; color: white; padding: 20px; }
    header h1 { float: left; }
    header nav { float: right; }
    header nav a { color: white; text-decoration: none; margin-left: 10px; }
    table { width: 100%; border-collapse: collapse; }
    table th, table td { border: 1px solid #ccc; padding: 10px; text-align: left; }
    table th { background-color: #f4f4f4; }
    .clear { clear: both; }
    footer { background: #333; color: white; text-align: center; padding: 10px; }
</style>
</head>
<body>
    <header>
        <div class="container">
            <h1>Прокат Велосипедов</h1>
            <nav>
                <a href="/">Главная</a>
                <a href="#">Регистрация</a>
                <a href="#">Выбор Велосипеда</a>
            </nav>
            <div class="clear"></div>
        </div>
    </header>

    <section class="container">
        <h2>Выбор Велосипеда</h2>
        <table>
            <tr>
                <th>Название</th>
                <th>Категория</th>
                <th>Цвет</th>
                <th>Размер колес</th>
                <th>Цена за час</th>
            </tr>
            <tr>
                <td>City Cruiser</td>
                <td>Для города</td>
                <td>Красный</td>
                <td>26"</td>
                <td>150 руб.</td>
            </tr>
            <tr>
                <td>Urban Wave</td>
                <td>Для города</td>
                <td>Синий</td>
                <td>28"</td>
                <td>150 руб.</td>
            </tr>
            <tr>
                <td>Mountain Hawk</td>
                <td>Горный</td>
                <td>Черный</td>
                <td>27.5"</td>
                <td>200 руб.</td>
            </tr>
            <tr>
                <td>Hill Climber</td>
                <td>Горный</td>
                <td>Зеленый</td>
                <td>29"</td>
                <td>200 руб.</td>
            </tr>
            <tr>
                <td>Road Racer Pro</td>
                <td>Дорожный</td>
                <td>Белый</td>
                <td>700C</td>
                <td>250 руб.</td>
            </tr>
        </table>
    </section>

    <footer>
        <div class="container">
            <p>© 2023 Прокат Велосипедов. Все права защищены.</p>
        </div>
    </footer>
</body>
</html>