<section class="container">
    <h2>Выбор Велосипеда</h2>
    <table>
        <tr>
            <th>Название</th>
            <th>Категория</th>
            <th>Цвет</th>
            <th>Размер колес</th>
            <th>Цена за час</th>
        </tr>
        <tr>
            <td>City Cruiser</td>
            <td>Для города</td>
            <td>Красный</td>
            <td>26"</td>
            <td>150 руб.</td>
        </tr>
        <tr>
            <td>Urban Wave</td>
            <td>Для города</td>
            <td>Синий</td>
            <td>28"</td>
            <td>150 руб.</td>
        </tr>
        <tr>
            <td>Mountain Hawk</td>
            <td>Горный</td>
            <td>Черный</td>
            <td>27.5"</td>
            <td>200 руб.</td>
        </tr>
        <tr>
            <td>Hill Climber</td>
            <td>Горный</td>
            <td>Зеленый</td>
            <td>29"</td>
            <td>200 руб.</td>
        </tr>
        <tr>
            <td>Road Racer Pro</td>
            <td>Дорожный</td>
            <td>Белый</td>
            <td>700C</td>
            <td>250 руб.</td>
        </tr>
    </table>
</section>