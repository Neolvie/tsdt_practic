<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Прокат Велосипедов</title>
<style>
    body { font-family: Arial, sans-serif; line-height: 1.6; }
    .container { width: 80%; margin: auto; overflow: hidden; }
    header { background: #333; color: white; padding: 20px; }
    header h1 { float: left; }
    header nav { float: right; }
    header nav a { color: white; text-decoration: none; margin-left: 10px; }
    section { padding: 20px; }
    footer { background: #333; color: white; text-align: center; padding: 10px; }
    .clear { clear: both; }
</style>
</head>
<body>
    <header>
        <div class="container">
            <h1>Прокат Велосипедов</h1>
            <nav>
                <a href="/">Главная</a>
                <a href="#">Регистрация</a>
                <a href="#">Выбор Велосипеда</a>
            </nav>
            <div class="clear"></div>
        </div>
    </header>

    <section class="container">
        <h2>О Нас</h2>
        <p>Добро пожаловать в наш сервис проката велосипедов! Мы предоставляем широкий выбор велосипедов для всех возрастов и предпочтений. Наслаждайтесь красивыми пейзажами и свежим воздухом на двух колесах!</p>

        <h2>Стоимость Проката</h2>
        <ul>
            <li>1 час — 150 рублей</li>
            <li>1 день — 800 рублей</li>
            <li>1 неделя — 4000 рублей</li>
        </ul>

        <h2>Контакты</h2>
        <p>Если у вас возникли вопросы, пожалуйста, свяжитесь с нами:</p>
        <p>Телефон: +7 123 456 78 90</p>
        <p>Email: info@velorent.ru</p>
    </section>

    <footer>
        <div class="container">
            <p>© 2023 Прокат Велосипедов. Все права защищены.</p>
        </div>
    </footer>
</body>
</html>