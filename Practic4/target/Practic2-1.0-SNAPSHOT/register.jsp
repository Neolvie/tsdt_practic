<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Регистрация пользователя</title>
<style>
    body { font-family: Arial, sans-serif; line-height: 1.6; }
    .container { width: 80%; margin: auto; overflow: hidden; }
    header { background: #333; color: white; padding: 20px; }
    header h1 { float: left; }
    header nav { float: right; }
    header nav a { color: white; text-decoration: none; margin-left: 10px; }
    .registration-form { background-color: #f4f4f4; padding: 20px; margin-top: 20px; }
    .registration-form h2 { margin-bottom: 20px; }
    .form-control { margin-bottom: 10px; }
    .form-control label { display: block; margin-bottom: 5px; }
    .form-control input { width: 100%; padding: 10px; border: 1px solid #ccc; }
    .form-control input[type="submit"] { background-color: #333; color: white; cursor: pointer; }
    .form-control input[type="submit"]:hover { background-color: #555; }
    .clear { clear: both; }
    footer { background: #333; color: white; text-align: center; padding: 10px; }
</style>
</head>
<body>
    <header>
        <div class="container">
            <h1>Прокат Велосипедов</h1>
            <nav>
                <a href="/">Главная</a>
                <a href="#">Регистрация</a>
                <a href="#">Выбор Велосипеда</a>
            </nav>
            <div class="clear"></div>
        </div>
    </header>

    <section class="container">
        <div class="registration-form">
            <h2>Форма Регистрации</h2>
            <form action="/submit_registration" method="post">
                <div class="form-control">
                    <label for="name">Имя:</label>
                    <input type="text" id="name" name="name" required>
                </div>
                <div class="form-control">
                    <label for="surname">Фамилия:</label>
                    <input type="text" id="surname" name="surname" required>
                </div>
                <div class="form-control">
                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email" required>
                </div>
                <div class="form-control">
                    <label for="password">Пароль:</label>
                    <input type="password" id="password" name="password" required>
                </div>
                <div class="form-control">
                    <input type="submit" value="Зарегистрироваться">
                </div>
            </form>
        </div>
    </section>

    <footer>
        <div class="container">
            <p>© 2023 Прокат Велосипедов. Все права защищены.</p>
        </div>
    </footer>
</body>
</html>