<section class="container">
    <h2>О Нас</h2>
    <p>Добро пожаловать в наш сервис проката велосипедов! Мы предоставляем широкий выбор велосипедов для всех возрастов и предпочтений. Наслаждайтесь красивыми пейзажами и свежим воздухом на двух колесах!</p>
    
    <h2>Стоимость Проката</h2>
    <ul>
        <li>1 час — 150 рублей</li>
        <li>1 день — 800 рублей</li>
        <li>1 неделя — 4000 рублей</li>
    </ul>

    <h2>Контакты</h2>
    <p>Если у вас возникли вопросы, пожалуйста, свяжитесь с нами:</p>
    <p>Телефон: +7 123 456 78 90</p>
    <p>Email: info@velorent.ru</p>
</section>