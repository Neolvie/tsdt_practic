<h2>Список пользователей</h2>
<table border="1">
    <thead>
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="user" items="${users}">
            <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.surname}</td>
                <td>${user.email}</td>
            </tr>
        </c:forEach>
    </tbody>
</table>

<h2>Список прокатов</h2>
<table border="1">
    <thead>
        <tr>
            <th>ID</th>
            <th>Велосипед</th>
            <th>Дата начала аренды</th>
            <th>Дата окончания аренды</th>
            <th>Сумма аренды</th>
            <th>Пользователь</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="rent" items="${rents}">
            <tr>
                <td>${rent.id}</td>
                <td>${rent.bike}</td>
                <td>${rent.rentFrom}</td>
                <td>${rent.rentTo}</td>
                <td>${rent.totalAmount}</td>
                <td>${rent.userId.surname} ${rent.userId.name} (${rent.userId.id})</td>
            </tr>
        </c:forEach>
    </tbody>
</table>