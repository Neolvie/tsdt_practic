/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB40/StatelessEjbClass.java to edit this template
 */
package com.belyak_is.practic4;

import jakarta.ejb.Stateless;
import jakarta.ejb.LocalBean;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.EntityManager;
import java.util.List;



/**
 *
 * @author user
 */
@Stateless
@LocalBean
public class DataService {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    public List<Users> getAllUsers() {
        return entityManager.createNamedQuery("Users.findAll", Users.class).getResultList();
    }
    
    public List<Rents> getAllRents() {
        return entityManager.createNamedQuery("Rents.findAll", Rents.class).getResultList();
    }
}
