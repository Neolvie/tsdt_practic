/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.belyak_is.practic4;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author user
 */
@Entity
@Table(name = "rents")
@NamedQueries({
    @NamedQuery(name = "Rents.findAll", query = "SELECT r FROM Rents r"),
    @NamedQuery(name = "Rents.findById", query = "SELECT r FROM Rents r WHERE r.id = :id"),
    @NamedQuery(name = "Rents.findByBike", query = "SELECT r FROM Rents r WHERE r.bike = :bike"),
    @NamedQuery(name = "Rents.findByRentFrom", query = "SELECT r FROM Rents r WHERE r.rentFrom = :rentFrom"),
    @NamedQuery(name = "Rents.findByRentTo", query = "SELECT r FROM Rents r WHERE r.rentTo = :rentTo"),
    @NamedQuery(name = "Rents.findByTotalAmount", query = "SELECT r FROM Rents r WHERE r.totalAmount = :totalAmount")})
public class Rents implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "bike")
    private String bike;
    @Column(name = "rent_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rentFrom;
    @Column(name = "rent_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rentTo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "total_amount")
    private Double totalAmount;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private Users userId;

    public Rents() {
    }

    public Rents(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBike() {
        return bike;
    }

    public void setBike(String bike) {
        this.bike = bike;
    }

    public Date getRentFrom() {
        return rentFrom;
    }

    public void setRentFrom(Date rentFrom) {
        this.rentFrom = rentFrom;
    }

    public Date getRentTo() {
        return rentTo;
    }

    public void setRentTo(Date rentTo) {
        this.rentTo = rentTo;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rents)) {
            return false;
        }
        Rents other = (Rents) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.belyak_is.practic4.Rents[ id=" + id + " ]";
    }
    
}
