<section class="container">
    <div class="registration-form">
        <h2>Форма Регистрации</h2>
        <form action="${currentUrl}/NewServlet" method="post">
            <div class="form-control">
                <label for="name">Имя:</label>
                <input type="text" id="name" name="name" required>
            </div>
            <div class="form-control">
                <label for="surname">Фамилия:</label>
                <input type="text" id="surname" name="surname" required>
            </div>
            <div class="form-control">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" required>
            </div>
            <div class="form-control">
                <label for="password">Пароль:</label>
                <input type="password" id="password" name="password" required>
            </div>
            <div class="form-control">
                <input type="submit" value="Зарегистрироваться">
            </div>
        </form>
    </div>
</section>